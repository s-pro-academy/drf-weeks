# Short description
Home tasks for S-PRO Academy Django REST framework part

# Stack
Python 3.8 + Django 4.0.6 + Django REST framework 3.13.1

# Gitflow
master – base project branch  
drf-week-*n* – master + home tasks *n* week