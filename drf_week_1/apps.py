from django.apps import AppConfig


class DrfWeek1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'drf_week_1'
